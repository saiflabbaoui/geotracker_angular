import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanLoad, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { TokenStorageService } from './token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanLoad, CanActivate {

  constructor(private tokenStorage: TokenStorageService, public auth: AuthService, public router: Router) { }


  canLoad() {
    const currentUser = this.auth.currentUserValue;
    if (currentUser) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/user']);
    return false;

  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.auth.currentUserValue;

    if (currentUser) {
      if (route.data.roles.includes(this.tokenStorage.getUser().roles[0])) {
        return true;

      }
      this.router.navigate(['/user']);

      // logged in so return true
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/user']);
    return false;

  }
}