import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Company } from '../models/company';
import { CompanyAdmin } from '../models/companyAdmin';
import { JwtResponse } from '../models/jwt-response';
import { LoginInfo } from '../models/login-info';
import { User } from '../models/user';
import { TokenStorageService } from './token-storage.service';


const httpOptions ={
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const TOKEN_KEY = 'AuthToken';
const CURRENT_USER = 'CurrentUser';
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           

@Injectable({
  providedIn: 'root'
})
export class AuthService {

public currentUserSubject: BehaviorSubject<any>;
public currentUser: Observable<any>;
private signupUrl = 'http://localhost:8080/api/auth/signup';
private loginUrl = 'http://localhost:8080/api/auth/signin';

  constructor(private http:HttpClient, private jwtHelper: JwtHelperService,
     private tokenStorage: TokenStorageService, private router: Router) {
       this.currentUserSubject = new BehaviorSubject<any>(sessionStorage.getItem(CURRENT_USER));
       this.currentUser = this.currentUserSubject.asObservable();
      }
      
    public get currentUserValue(): any {
     return this.currentUserSubject.value;
    }

     signUp(companyAdmin: CompanyAdmin){

      return this.http.post<JwtResponse>(this.signupUrl, companyAdmin, httpOptions).pipe(map(data =>{
        this.saveUserData(data);
        return data;
      }));
     }
     private saveUserData(data){
       this.tokenStorage.saveToken(data.accessToken);
       this.tokenStorage.saveUsername(data.userName);
       this.tokenStorage.saveAuthorities(data.roles);
       this.tokenStorage.saveUser(data);
       this.currentUserSubject.next(data.accessToken)
      
     }

     login(authloginInfo: LoginInfo){
      return this.http.post<JwtResponse>(this.loginUrl, authloginInfo, httpOptions).pipe(map(data =>{
        this.saveUserData(data);
        return data;
      }));

     }

     isUserLoggedIn() : boolean {
      const token = sessionStorage.getItem('AuthToken');
      // Check whether the token is expired and return
      // true or false
      return !this.jwtHelper.isTokenExpired(token);
    }

    logOut() {
    return sessionStorage.clear();
    }
     
}