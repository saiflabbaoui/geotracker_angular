import { Injectable } from '@angular/core';
import { User } from '../models/user';




const TOKEN_KEY = 'AuthToken';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';
const CURRENT_USER = 'CurrentUser';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

private roles: Array<string> = [];
private currentUser: any;


  constructor() {
      }
    public saveToken(token: string){
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token); 
    }
    public getToken() : string {
        return sessionStorage.getItem(TOKEN_KEY);
    }

    public saveUsername(username: string){
        window.sessionStorage.removeItem(USERNAME_KEY);
        window.sessionStorage.setItem(USERNAME_KEY, username); 
    }
    public getUsername() : string {
        return sessionStorage.getItem(USERNAME_KEY);
    }
    public saveAuthorities(authorities: string[]){
        window.sessionStorage.removeItem(AUTHORITIES_KEY);
        window.sessionStorage.setItem(AUTHORITIES_KEY, JSON.stringify(authorities) ); 
    }
    public getAuthorities() : string[] {
        this.roles = [];
        if (sessionStorage.getItem(TOKEN_KEY)) {
    
         JSON.parse(sessionStorage.getItem(AUTHORITIES_KEY)).foreach(authority =>{
            this.roles.push(authority.authority)
        });
    }
    
        return this.roles;
    }

    public saveUser(user: any){
        window.sessionStorage.removeItem(CURRENT_USER);
        window.sessionStorage.setItem(CURRENT_USER, JSON.stringify(user) ); 
    }

    public getUser() : any {
        this.currentUser  ;
        if (sessionStorage.getItem(CURRENT_USER)) {
    
            return  JSON.parse(sessionStorage.getItem(CURRENT_USER));
        }
    
    }
    }