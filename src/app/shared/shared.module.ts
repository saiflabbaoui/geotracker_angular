import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from '../views/error/error.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AddSiteComponent } from '../views/app/sites/add-site/add-site.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
@NgModule({
  declarations: [ErrorComponent,AddSiteComponent],
  imports: [
    RouterModule,
    CommonModule,
    TranslateModule,
    PerfectScrollbarModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDXp2UQHy2S-pyPqniPA--wSYM-MrLCdGg'
    }),
    ReactiveFormsModule,


  ],
  exports: [
    PerfectScrollbarModule,
    RouterModule,
    ErrorComponent,
    TranslateModule,
    CommonModule,
    AddSiteComponent,
    ReactiveFormsModule,

  ]
})
export class SharedModule { }
