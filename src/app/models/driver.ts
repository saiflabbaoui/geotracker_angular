import { Company } from "./company";
import { Mission } from "./mission";
import { Truck } from "./truck";

export class Driver {
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    phoneNumber: number;
    email: string;
    truck: Truck;
    mission: Mission;
    company: Company;
}
    



    
    

