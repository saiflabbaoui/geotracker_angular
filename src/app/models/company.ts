export class Company {
    id: number;
    socialReason: string;
    address: string;
    phoneNumber: number;
    nbrUsers: number;
    nbrDrivers:number;
    nbrTrucks: number;
 
}