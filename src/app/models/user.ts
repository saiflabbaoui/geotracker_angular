import { Company } from "./company";

export class User {
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    password: string;
    company: Company;
    role: string[];
    token?: string;



    
    constructor(firstName: string, lastName: string, userName: string, email: string, password: string, company: Company){
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.company = company;
        this.role = ['ROLE_ADMIN'];
    }

}