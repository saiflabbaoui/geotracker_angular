import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { TopnavComponent } from './topnav/topnav.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { ColorSwitcherComponent } from './color-switcher/color-switcher.component';
import { CollapseModule, BsDropdownModule, TooltipModule, ModalModule } from 'ngx-bootstrap';
import { FooterComponent } from './footer/footer.component';
import { HeadingComponent } from './heading/heading.component';
import { ApplicationMenuComponent } from './application-menu/application-menu.component';
import { FormsModule } from '@angular/forms';
import { SidebarSuperAdminComponent } from './sidebar/sidebarSuperAdmin.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProfilComponent } from 'src/app/views/app/profil/profil.component';
import { ArchwizardModule } from 'angular-archwizard';
import { AddUserComponent } from 'src/app/views/app/add-user/add-user.component';

@NgModule({
  declarations: [
    TopnavComponent,
    SidebarComponent,
    BreadcrumbComponent,
    ColorSwitcherComponent,
    FooterComponent,
    HeadingComponent,
    ApplicationMenuComponent,
    SidebarSuperAdminComponent,
    ProfilComponent,
    AddUserComponent
  ],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    TranslateModule,
    RouterModule,
    CollapseModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    SharedModule,
    ArchwizardModule,
    ModalModule.forRoot(),

    
  ],
  exports: [
    TopnavComponent,
    SidebarComponent,
    BreadcrumbComponent,
    ColorSwitcherComponent,
    FooterComponent,
    HeadingComponent,
    ApplicationMenuComponent,
    SidebarSuperAdminComponent
  ]
})
export class LayoutContainersModule { }
