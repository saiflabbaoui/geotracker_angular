import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { BehaviorSubject, Observable, throwError } from 'rxjs'; 
import { catchError, map } from 'rxjs/operators';
import { Truck } from 'src/app/models/truck';



export interface ITruckResponse {
  data: Truck[];
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}


@Injectable({ providedIn: 'root' }) 
export class TruckService{ 


  private baseUrl = 'http://localhost:8080/api/v1/truck'; 
 
  truckIdSource = new  BehaviorSubject<number>(0);

  constructor(private httpClient: HttpClient) { } 
  
  
  addTruck(truck: Truck):Observable<Truck> { 
    return this.httpClient.post<Truck>(this.baseUrl, truck); 
  } 
  
  getAllTrucks(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = ''): Observable<ITruckResponse> {
    let params = new HttpParams();
    params = params.append('pageSize', pageSize + '');
    params = params.append('currentPage', currentPage + '');
    params = params.append('search', search);
    params = params.append('orderBy', orderBy);
    return this.httpClient.get('http://localhost:8080/api/v1/trucks/pageanable', { params })
    .pipe(
      map((res: ITruckResponse) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }
  
  getAll(): Observable<Truck[]> {
    return this.httpClient.get('http://localhost:8080/api/v1/trucks').pipe(
      map((res: Truck[]) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );;
  }

  getTruck(id: number): Observable<any> {
    return this.httpClient.get('http://localhost:8080/api/v1/truck?id='+id);
  }

   updateTruck(truck: Truck): Observable<any>{

    return this.httpClient.put('http://localhost:8080/api/v1/truck/', truck);
   }
   deleteTruck(id: number): Observable<any>{

    return this.httpClient.delete('http://localhost:8080/api/v1/truck?id='+id);
   }
}