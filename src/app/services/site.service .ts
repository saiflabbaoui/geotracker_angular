import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { Observable, throwError } from 'rxjs'; 
import { catchError, map } from 'rxjs/operators';
import { Site } from '../models/site';



export interface ISiteResponse {
  site: Site[];
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}


@Injectable({ providedIn: 'root' }) 
export class SiteService{ 


  private baseUrl = 'http://localhost:8080/api/v1/site'; 
  
  constructor(private httpClient: HttpClient) { } 
  
  
  addSite(site: Site):Observable<any> { 
    return this.httpClient.post(this.baseUrl,site); 
  } 
  getAllSitesPagean(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = ''): Observable<any> {
    let params = new HttpParams();
    params = params.append('pageSize', pageSize + '');
    params = params.append('currentPage', currentPage + '');
    params = params.append('search', search);
    params = params.append('orderBy', orderBy);
    return this.httpClient.get('http://localhost:8080/api/v1/sites/pageanable', { params })
    .pipe(
      map((res: ISiteResponse) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }

  getAllSites(): Observable<any> {
   
    return this.httpClient.get('http://localhost:8080/api/v1/sites').pipe(
      map((res: ISiteResponse) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }
  
  getAll(): Observable<Site[]> {
    return this.httpClient.get('http://localhost:8080/api/v1/sites').pipe(
      map((res: Site[]) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );;
  }

  updateSite(site: Site): Observable<any>{

    return this.httpClient.put('http://localhost:8080/api/v1/site/', site);
   }
   deleteSite(id: number): Observable<any>{

    return this.httpClient.delete('http://localhost:8080/api/v1/site?id='+id);
   }
}