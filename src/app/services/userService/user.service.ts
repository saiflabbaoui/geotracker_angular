import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Company } from 'src/app/models/company';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>(`users`);
}

getUserById(id: number) : Observable<User> {
    return this.http.get<User>('http://localhost:8080/api/v1/user?idUser='+id);
}

getUsersByCompany(idCompany:number): Observable<User[]> {
    
  return this.http.get('http://localhost:8080/api/v1/users?idCompany='+idCompany)
  .pipe(
    map((res: User[]) => {
      return res;
    }),
    catchError(errorRes => {
      return throwError(errorRes);
    })
  );
}
addUserToCompany(user: User) : Observable<User> {
  return this.http.post<User>('http://localhost:8080/api/auth/adduser',user);
}
updateProfil(user: User): Observable<any>{

  return this.http.put('http://localhost:8080/api/v1/profil/', user);
 }

}
