import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { Observable, throwError } from 'rxjs'; 
import { catchError, map } from 'rxjs/operators';
import { Mission } from '../models/mission';
import { Site } from '../models/site';



export interface IMissionResponse {
  mission: Mission[];
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}


@Injectable({ providedIn: 'root' }) 
export class MissionService{ 


  private baseUrl = 'http://localhost:8080/api/v1/mission'; 
  
  constructor(private httpClient: HttpClient) { } 
  
  
  addMission(mission: Mission,sites:Site[]):Observable<any> { 
    return this.httpClient.post(this.baseUrl,{mission,sites}); 
  } 

  getAllMissions(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = ''): Observable<any> {
    let params = new HttpParams();
    params = params.append('pageSize', pageSize + '');
    params = params.append('currentPage', currentPage + '');
    params = params.append('search', search);
    params = params.append('orderBy', orderBy);
    return this.httpClient.get('http://localhost:8080/api/v1/missions/pageanable', { params })
    .pipe(
      map((res: IMissionResponse) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }
  
  getAll(): Observable<Mission[]> {
    
    return this.httpClient.get('http://localhost:8080/api/v1/missions')
    .pipe(
      map((res: Mission[]) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }
  nbrMissionsEnCours(){

    return this.httpClient.get('http://localhost:8080/api/v1/mission/nbrencours' );
   }
   nbrMissionsEnAttente(){

    return this.httpClient.get('http://localhost:8080/api/v1/mission/nbrenattente' );
   }
   nbrMissionsTermine(){

    return this.httpClient.get('http://localhost:8080/api/v1/mission/nbrtermine' );
   }

  updateMission(mission: Mission): Observable<any>{

    return this.httpClient.put('http://localhost:8080/api/v1/mission/', mission);
   }

}