import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs'; 
import { Driver } from '../models/driver';
import { catchError, map } from 'rxjs/operators';


export interface IDriverResponse {
  data: Driver[];
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  private baseUrl = 'http://localhost:8080/api/v1/driver'; 


  constructor(private httpClient: HttpClient) { } 
  
  
  addDriver(driver: Driver):Observable<any> { 
    return this.httpClient.post(this.baseUrl, driver); 
  } 
  
  getAlldrivers(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = ''): Observable<any> {
    let params = new HttpParams();
    params = params.append('pageSize', pageSize + '');
    params = params.append('currentPage', currentPage + '');
    params = params.append('search', search);
    params = params.append('orderBy', orderBy);
    return this.httpClient.get('http://localhost:8080/api/v1/drivers/pageanable', { params })
    .pipe(
      map((res: IDriverResponse) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }

  getAll(): Observable<Driver[]> {
    
    return this.httpClient.get('http://localhost:8080/api/v1/drivers')
    .pipe(
      map((res: Driver[]) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }

  updateDriver(driver: Driver): Observable<any>{

    return this.httpClient.put('http://localhost:8080/api/v1/driver/', driver);
   }
   deleteDriver(id: number): Observable<any>{

    return this.httpClient.delete('http://localhost:8080/api/v1/driver?id='+id);
   }
}
