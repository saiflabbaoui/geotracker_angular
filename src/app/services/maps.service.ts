import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { Observable, throwError } from 'rxjs'; 
import { catchError, map } from 'rxjs/operators';
import { Mission } from '../models/mission';
import { Site } from '../models/site';
import { Truck } from '../models/truck';
import { TruckInformation } from '../models/truckInformation';



export interface IMissionResponse {
  mission: Mission[];
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}


@Injectable({ providedIn: 'root' }) 
export class MapsService{ 


  private baseUrl = 'http://localhost:8080/api/v1/mission'; 
  
  constructor(private httpClient: HttpClient) { } 
  
  
  

  getTruckInformations(truck: Truck): Observable<any>{

    return this.httpClient.post('http://localhost:8080/api/v1/truckinformation/', truck);
   }

   getLastTruckInfo(truckId : number) : Observable<TruckInformation> {
     
     return this.httpClient.get<TruckInformation>('http://localhost:8080/api/v1/missions/getLastTruckInformation?idTruck='+truckId);
   }

}