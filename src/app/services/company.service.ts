import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs'; 
import { Driver } from '../models/driver';
import { catchError, map } from 'rxjs/operators';
import { Company } from '../models/company';
import { User } from '../models/user';


export interface IDriverResponse {
  data: Driver[];
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private baseUrl = 'http://localhost:8080/api/v1/company'; 


  constructor(private httpClient: HttpClient) { } 
  
  
  addCompany(user: User,company: Company):Observable<any> { 
    return this.httpClient.post('http://localhost:8080/api/auth/signup', {user, company}); 
  } 
  
 

  getAll(): Observable<Company[]> {
    
    return this.httpClient.get('http://localhost:8080/api/v1/companies')
    .pipe(
      map((res: Company[]) => {
        return res;
      }),
      catchError(errorRes => {
        return throwError(errorRes);
      })
    );
  }

  updateCompany(company: Company): Observable<any>{

    return this.httpClient.put('http://localhost:8080/api/v1/company/', company);
   }

}
