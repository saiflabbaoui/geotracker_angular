import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewsComponent } from './views.component';
import { ErrorComponent } from './error/error.component';
import { environment } from './../../environments/environment';
import { AuthGuardGuard } from '../shared/auth-guard.guard';
import { AppComponent } from './app/app.component';
import { Role } from '../models/role.user';
import { SuperAdminComponent } from './backOffice/superAdmin/superAdmin.component';


const routes: Routes = [
  {
    path: '',
    component: ViewsComponent,
    pathMatch: 'full',
  },
  { path: 'app', component: AppComponent, loadChildren: () => import('./app/app.module').then(m => m.AppModule), canActivate: [AuthGuardGuard],data: { roles: [Role.Admin,Role.User]} },
  { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule)},
  { path: 'superAdmin', component: SuperAdminComponent, loadChildren: () => import('./backOffice/superAdmin/superAdmin.module').then(m => m.SuperAdminModule), canLoad: [AuthGuardGuard],data: { roles: [Role.SuperAdmin]} },
  { path: 'error', component: ErrorComponent },
  { path: '**', redirectTo: '/error' }
];

 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRoutingModule { }
