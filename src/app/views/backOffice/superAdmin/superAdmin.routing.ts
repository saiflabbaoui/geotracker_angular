import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCompaniesComponent } from './list-companies/list-companies.component';
import { SuperAdminComponent } from './superAdmin.component';


const routes: Routes = [
    {
        path: '', component: SuperAdminComponent,
        children: [
            { path: '', redirectTo: 'companies', pathMatch: 'full' },
            { path: 'companies', component: ListCompaniesComponent }
            
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SuperAdminRoutingModule { }
