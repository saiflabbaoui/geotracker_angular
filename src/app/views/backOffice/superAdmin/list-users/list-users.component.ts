import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Company } from 'src/app/models/company';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html'
})
export class ListUsersComponent implements OnInit {
  modalRef: BsModalRef;
  users: User[] = [];
  company: Company = new Company();


  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-center'
  };

  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService, private userService: UserService) { }

  ngOnInit() {
    console.log("compaaaaaany",this.company);

  }
  show(company: Company) {

    this.company = company;
    this.getUsersByCompany(this.company.id);

    this.modalRef = this.modalService.show(this.template, this.config);

  }
  getUsersByCompany(idCompany: number) {

    this.userService.getUsersByCompany(idCompany).subscribe(data => {
      this.users = data;
      console.log("zzzzz", this.users);
    })
  }
}
