import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ISidebar, SidebarService } from 'src/app/containers/layout/sidebar/sidebar.service';

@Component({
  selector: 'app-superAdmin',
  templateUrl: './superAdmin.component.html'
})
export class SuperAdminComponent implements OnInit, OnDestroy  {
    sidebar: ISidebar;
    subscription: Subscription;

  constructor(private sidebarService: SidebarService) { }

  ngOnInit() {
    this.subscription = this.sidebarService.getSidebar().subscribe(
      res => {
        this.sidebar = res;
      },
      err => {
        console.error(`An error occurred: ${err.message}`);
      }
    );
   
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  
}