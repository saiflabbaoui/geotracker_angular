import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { CollapseModule, ModalModule } from "ngx-bootstrap";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { LayoutContainersModule } from "src/app/containers/layout/layout.containers.module";
import { SharedModule } from "src/app/shared/shared.module";
import { ListCompaniesComponent } from "./list-companies/list-companies.component";
import { SuperAdminComponent } from "./superAdmin.component";
import { SuperAdminRoutingModule } from "./superAdmin.routing";
import { ListUsersComponent } from './list-users/list-users.component';
import { HeaderSuperAdminComponent } from './header-super-admin/header-super-admin.component';
import { AddCompanyComponent } from './add-company/add-company.component';
import { NgSelectModule } from "@ng-select/ng-select";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ArchwizardModule } from "angular-archwizard";
import { SimpleNotificationsModule } from "angular2-notifications";

@NgModule({
    declarations: [SuperAdminComponent, ListCompaniesComponent, ListUsersComponent, HeaderSuperAdminComponent, AddCompanyComponent],
    imports: [
      CommonModule,
      SuperAdminRoutingModule,
      PerfectScrollbarModule,
      TranslateModule,
      CollapseModule.forRoot(),
      LayoutContainersModule,
      SharedModule,
      ModalModule.forRoot(),
      NgSelectModule,
      FormsModule,
      ArchwizardModule,
      SimpleNotificationsModule.forRoot()


    ],providers: [DatePipe]
  })
  export class SuperAdminModule { }
  