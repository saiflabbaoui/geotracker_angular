import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Company } from 'src/app/models/company';
import { CompanyService } from 'src/app/services/company.service';
import { AddCompanyComponent } from '../add-company/add-company.component';
import { ListUsersComponent } from '../list-users/list-users.component';

@Component({
  selector: 'app-list-companies',
  templateUrl: './list-companies.component.html'
})
export class ListCompaniesComponent implements OnInit {
  itemsPerPage = 5;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  displayMode = 'list';
  selectAllState = '';
  company: Company[] = [];
  @ViewChild('showUsersModalRef', { static: true }) showUsersModalRef: ListUsersComponent;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddCompanyComponent;

  constructor(private companyService: CompanyService) { }

  ngOnInit() {
    this.getAllCompanies();
  }
  getAllCompanies() {
    this.companyService.getAll().subscribe(data => {
      this.company = data;
    })
  }
  showUsers(c:Company){
    this.showUsersModalRef.show(c);

  }
  changeDisplayMode(mode) {
    this.displayMode = mode;
  }

  showAddNewModal() {
    this.addNewModalRef.show();
  }
  itemsPerPageChange(perPage: number) {
   // this.loadData(perPage, 1, this.search, this.orderBy);
  }
  changeOrderBy(item: any) {
   // this.loadData(this.itemsPerPage, 1, this.search, item.value);
  }

  searchKeyUp(event) {
    const val = event.target.value.toLowerCase().trim();
    //this.loadData(this.itemsPerPage, 1, val, this.orderBy);
  }
  selectAllChange($event) {
   // if ($event.target.checked) {
    //  this.selected = [...this.mission];
   // } else {
    //  this.selected = [];
   // }
   // this.setSelectAllState();
  }
  onStateChange(c:Company) {
    this.company.push(c);
  
  }
}
