import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { first } from 'rxjs/operators';
import { Company } from 'src/app/models/company';
import { CompanyAdmin } from 'src/app/models/companyAdmin';
import { User } from 'src/app/models/user';
import { CompanyService } from 'src/app/services/company.service';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html'
})
export class AddCompanyComponent implements OnInit {
  @Output() listCompany = new EventEmitter<Company>();


  buttonDisabled = false;
  buttonState = '';
  form: any= {};
  user: User;
  isSignUpFailed = false;
  company: Company = new Company();
  submitted = false;
  companyAdmin: CompanyAdmin = new CompanyAdmin;
  
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-center'
  };

  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor( private notifications: NotificationsService,private authService: AuthService, private modalService: BsModalService) { }

  ngOnInit() {

  }



  save() {

    this.user= new User(this.form.firstName, this.form.lastName, this.form.userName, this.form.email, this.form.password,this.company) ;
    this.companyAdmin.company=this.company;
    this.companyAdmin.user=this.user;
    this.authService.signUp(this.companyAdmin).pipe(first()).subscribe(data => {
      this.hide();
      
    }, (error) => {
        this.notifications.create('Error', error.message, NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
        this.buttonDisabled = false;
        this.buttonState = '';
        this.isSignUpFailed = true;
  
      });
    }
  onSubmit() {
    this.submitted = true;
    this.save();    
  }
  show() {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
 hide(){
   if(this.company.id!=null){
     this.listCompany.emit(this.company);
   }
   this.modalRef.hide();
 }
}
