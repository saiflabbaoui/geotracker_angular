import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewsComponent } from './views.component';
import { ViewRoutingModule } from './views.routing';
import { SharedModule } from '../shared/shared.module';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { JwtModule } from "@auth0/angular-jwt";
import { AuthGuardGuard } from '../shared/auth-guard.guard';
import { JwtInterceptor } from '../shared/jwt.interceptor';
import { ErrorInterceptor } from '../shared/error.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

export function tokenGetter() {
  return sessionStorage.getItem("AuthToken");
}

@NgModule({
  declarations: [ViewsComponent],
  imports: [
    CommonModule,
    ViewRoutingModule,
    SharedModule,
    AngularFireAuthModule,
    AngularFireAuthGuardModule,
    JwtModule.forRoot({
      config: {
       tokenGetter: tokenGetter,
      },
    }),
  ],
  providers: [
    AuthGuardGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ]

})
export class ViewsModule { }
