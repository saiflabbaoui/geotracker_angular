import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {
  @ViewChild('f') registerForm: NgForm;
  buttonDisabled = false;
  buttonState = '';
  form: any= {};
  user: User;
  isSignUpFailed = false;
  constructor(private authService: AuthService, private notifications: NotificationsService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    if (!this.registerForm.valid || this.buttonDisabled) {
      return;
    }
    this.buttonDisabled = true;
    this.buttonState = 'show-spinner';

   // this.user = new User(this.form.firstName, this.form.lastName, this.form.userName, this.form.email, this.form.password);
    //this.authService.signUp(this.user).pipe(first()).subscribe(
    //  data => {
    //    this.isSignUpFailed = false;
    //    this.router.navigate(['/user/login']);
    //  }, (error) => {
    //  this.notifications.create('Error', error.message, NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
    //  this.buttonDisabled = false;
    //  this.buttonState = '';
    //  this.isSignUpFailed = true;

    //});
  }
}
