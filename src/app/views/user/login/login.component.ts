import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { TokenStorageService } from 'src/app/shared/token-storage.service';
import { LoginInfo } from 'src/app/models/login-info';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  @ViewChild('f') loginForm: NgForm;
 
  form: any= {};
  isLoginFailed = false;
  buttonDisabled = false;
  buttonState = '';

  constructor(private authService: AuthService, private notifications: NotificationsService, private router: Router, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (!this.loginForm.valid || this.buttonDisabled) {
      return; 
    }
    this.buttonDisabled = true;
    this.buttonState = 'show-spinner';

    this.authService.login(new LoginInfo(this.form.userName, this.form.password)).pipe(first()).subscribe(
      data =>{
        this.isLoginFailed = false;
        if (data['roles'][0]=="ROLE_USER"||data['roles'][0]=="ROLE_ADMIN") {
          this.router.navigateByUrl('/app')
        }else  {
          this.router.navigateByUrl('/superAdmin')
        }
      }, (error) => {
        this.isLoginFailed = true;
      this.buttonDisabled = false;
      this.buttonState = '';
      this.notifications.create('Error', error.message, NotificationType.Bare, { theClass: 'outline primary', timeOut: 6000, showProgressBar: false });
    });
  }
}
