import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotkeyModule } from 'angular2-hotkeys';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { SortablejsModule } from 'ngx-sortablejs';
import { CollapseModule, TabsModule, ProgressbarModule, ModalModule, PaginationModule, AccordionModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { ContextMenuModule } from 'ngx-contextmenu';
import { AgmCoreModule } from '@agm/core';
import {AgGridModule} from "ag-grid-angular";
import { MapsComponent } from './maps/maps.component';
import { HistoricMapComponent } from './historic-map/historic-map.component';
import { MissionsRoutingModule } from '../missions/missions.routing';
import { TrackingRoutingModule } from './tracking.routing';
import { TrackingComponent } from './tracking.component';
//import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';


@NgModule({
  declarations: [TrackingComponent, MapsComponent, HistoricMapComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TrackingRoutingModule,
    LayoutContainersModule,
    ApplicationsContainersModule,
    ComponentsChartModule,
    SortablejsModule,
    HotkeyModule.forRoot(),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    NgSelectModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true,
    }),
    PaginationModule.forRoot(),
    //NgxMatDatetimePickerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDXp2UQHy2S-pyPqniPA--wSYM-MrLCdGg'
    }),
    AgGridModule.withComponents([]),
    AccordionModule.forRoot(),



  ],providers: [DatePipe]

})
export class TrackingModule { }
