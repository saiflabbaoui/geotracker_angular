import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoricMapComponent } from './historic-map/historic-map.component';
import { MapsComponent } from './maps/maps.component';
import { TrackingComponent } from './tracking.component';

const routes: Routes = [
    {
        path: '', component: TrackingComponent,
        children: [
           
            { path: 'map', component: MapsComponent },
            { path: 'historic', component: HistoricMapComponent },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TrackingRoutingModule { }
