import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricMapComponent } from './historic-map.component';

describe('HistoricMapComponent', () => {
  let component: HistoricMapComponent;
  let fixture: ComponentFixture<HistoricMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
