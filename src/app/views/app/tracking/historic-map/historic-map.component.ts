import { LatLng, MapsAPILoader } from '@agm/core';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { mergeMap, switchMap, tap } from 'rxjs/operators';
import { Mission } from 'src/app/models/mission';
import { Site } from 'src/app/models/site';
import { TruckInformation } from 'src/app/models/truckInformation';
import { MapsService } from 'src/app/services/maps.service';
import { MissionService } from 'src/app/services/mission.service';

@Component({
  selector: 'app-historic-map',
  templateUrl: './historic-map.component.html',
  styleUrls: ['./historic-map.component.scss']
})
export class HistoricMapComponent implements OnInit {
  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;  
  missions: Mission[];
  selectedMission: Mission = new Mission();
  siteDepart : Site;
  siteArrive : Site;
  sitesDepart : Site[] = [];
  sitesArrive : Site[] = [];
  selectedTruckInfo : TruckInformation[] = [];

  stations : Site[] = [];
  trajets : LatLng[];
  @ViewChild('search')
  public searchElementRef: ElementRef;
  
       
  
  start_end_mark = [];

  latlng = [];
  constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private missionService: MissionService, private mapsService : MapsService) {
    
   }
  Trucks: any[];
  TrucksPosition = [];


  iconPoint = {
    url : 'assets/img/point.png',
    scaledSize : {height: 10,width:10}
  }

  iconStation = {
    url : 'assets/img/gas_tank.png',
    scaledSize : {height: 38.23,width:29.92}
  }
  iconDeparture = {
    url : 'assets/img/Departure_mission.png',
    scaledSize : {height: 38.23,width:29.92}
  }
  iconArrive = {
    url : 'assets/img/end_flag.png',
    scaledSize : {height: 38.23,width:29.92}
  }
  previous;
  selectedTruck: any=null;
 
  ngOnInit() {

    this.mapsAPILoader.load().then(() => {
      // this.setCurrentLocation();

     /*  this.geoCoder = new google.maps.Geocoder;
  
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
  
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
  
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      }); */

      this.missionService.getAll().subscribe(data => {
        this.missions = data.filter(m=>m.status=="Terminé");
console.log(this.missions)
      })
    });

    this.Trucks = [
      {truckName:"Scania 11236",latitude:"36.37388824300486",longitude:"9.798867230656723"},
      {truckName:"Scania 11000",latitude:"35.83821079506242",longitude:"9.6235037807005"},
      {truckName:"Scania 11111",latitude:"35.776689700302846",longitude:"9.79492647959099"}
  ]
  }

/* private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  } */
  
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
  
    });
  }

  mapClicked($event: MouseEvent) {

  }

  clickedMarkerTrucks(infowindow, truck: any, index: number) {
    if (this.previous) {
      this.previous.close();
      // this.showPositionVessel = !this.showPositionVessel;
    }
    this.previous = infowindow;
    this.selectedTruck = truck;
    //this.getVesselDetail(label);
  }

  selectMission(m : Mission) {
    this.selectedMission = m;
    this.sitesDepart = [];
    this.sitesArrive = [];
    this.stations = [];
    this.latlng =[];
    this.sitesDepart.push(this.selectedMission.sites[0]);
  
    this.sitesArrive.push(this.selectedMission.sites[this.selectedMission.sites.length-1]);
   

for(var _i = 0; _i < this.selectedMission.sites.length; _i++) {
  if(_i !== 0 && _i !== this.selectedMission.sites.length-1 ) {
    this.stations.push(this.selectedMission.sites[_i]);
  }
}

    this.mapsService.getTruckInformations(m.drivers[0].truck).subscribe(data=>{
      
      this.selectedTruckInfo =data;
  
      this.selectedTruckInfo.forEach(element => {
        this.latlng.push([element.latitude,element.longitude])
        this.TrucksPosition = [];
        this.TrucksPosition.push([element.latitude,element.longitude]);
        this.getLastTruckInfo(m.drivers[0].truck.id);
      });
      
      
    })
    
  } 

  getLastTruckInfo(truckId:number) {
  
    interval(30000).pipe(
      mergeMap(()=>this.mapsService.getLastTruckInfo(truckId))).
      subscribe(data=> {
        this.latlng.push([data.latitude,data.longitude])
        this.TrucksPosition = [];
        this.TrucksPosition.push([data.latitude,data.longitude]);
      
    });
  }



}
