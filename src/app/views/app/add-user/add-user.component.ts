import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/userService/user.service';
import { AuthService } from 'src/app/shared/auth.service';
import { TokenStorageService } from 'src/app/shared/token-storage.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html'
})
export class AddUserComponent implements OnInit {
  form: any = {};
  user: User;
  submitted = false;
  currentUser: User;
  error;
  addUserFrom: FormGroup;


  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-center',

  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private userService: UserService, private tokenStorage: TokenStorageService,private formBuilder: FormBuilder) {
    this.getCurrentUser(this.tokenStorage.getUser());
  }

  ngOnInit() {

    this.addUserFrom = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required ]
    
      });

  }
  get f() { return this.addUserFrom.controls; }

  show() {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
  save() {
    this.user = new User(this.form.firstName, this.form.lastName, this.form.userName, this.form.email, this.form.password, this.currentUser.company)
    this.error = null
    this.userService
      .addUserToCompany(this.user).subscribe(data => {
        this.user = data;
        this.modalRef.hide();
        this.addUserFrom.reset();
      }, (error) => {
        this.error = error});

  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  getCurrentUser(user: any) {
    this.userService.getUserById(user['id']).subscribe(user => {
      this.currentUser = user;
    });
  }
}
