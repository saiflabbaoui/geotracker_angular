import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { MissionsRoutingModule } from './missions.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotkeyModule } from 'angular2-hotkeys';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { SortablejsModule } from 'ngx-sortablejs';
import { CollapseModule, TabsModule, ProgressbarModule, ModalModule, PaginationModule, AccordionModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { MissionsComponent } from './missions.component';
import { ListMissionsComponent } from './list-missions/list-missions.component';
import { AddMissionComponent } from './add-mission/add-mission.component';
import { MissionHeaderComponent } from './mission-header/mission-header.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { AddSiteComponent } from '../sites/add-site/add-site.component';
import { AgmCoreModule } from '@agm/core';
import {AgGridModule} from "ag-grid-angular";
import { ComponentsModule } from '../ui/components/components.module';
import { UpdateMissionComponent } from './update-mission/update-mission.component';
//import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';


@NgModule({
  declarations: [MissionsComponent, ListMissionsComponent, AddMissionComponent, MissionHeaderComponent, UpdateMissionComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    MissionsRoutingModule,
    LayoutContainersModule,
    ApplicationsContainersModule,
    ComponentsChartModule,
    SortablejsModule,
    HotkeyModule.forRoot(),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    NgSelectModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true,
    }),
    PaginationModule.forRoot(),
    //NgxMatDatetimePickerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDXp2UQHy2S-pyPqniPA--wSYM-MrLCdGg'
    }),
    AgGridModule.withComponents([]),
    AccordionModule.forRoot(),



  ],providers: [DatePipe]

})
export class MissionsModule { }
