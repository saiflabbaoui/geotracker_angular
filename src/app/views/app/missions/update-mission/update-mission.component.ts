import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsLocaleService, BsModalRef, BsModalService, defineLocale, esLocale } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { Driver } from 'src/app/models/driver';
import { Mission } from 'src/app/models/mission';
import { Site } from 'src/app/models/site';
import { Truck } from 'src/app/models/truck';
import { DriverService } from 'src/app/services/driver.service';
import { MissionService } from 'src/app/services/mission.service';
import { SiteService } from 'src/app/services/site.service ';
import { TruckService } from 'src/app/services/truckService/truck.service';

@Component({
  selector: 'app-update-mission',
  templateUrl: './update-mission.component.html'
})
export class UpdateMissionComponent implements OnInit {

  @Output() addNewItem: EventEmitter<any> = new EventEmitter();

  submitted = false;

  bsValue = new Date();
  missionStartDate: Date[];
  missionEndDate: Date[];

  maxDate = new Date();
  bsInlineValue = new Date();

  githubUsers$: Observable<any[]>;
  selectedUsers = [];

  currentPage = 1;
  itemsPerPage = 5;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  truck: Truck[] = [];
  truckmission: Truck
  driver: Driver[] = [];
  site: Site[];
  mission: Mission = new Mission();
  site1: Site;
  site2: Site;
  site3: Site[];

  trucks = [];
  drivers = [];
  sites = [];
  drs: Driver[] = [];

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-left'
  };

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private truckService: TruckService,
    private driverService: DriverService, private siteService: SiteService, private missionService: MissionService, private router: Router) {


    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.missionStartDate = [this.bsValue, this.maxDate];
    this.missionEndDate = [this.bsValue, this.maxDate];
    defineLocale('es', esLocale);
    // this.localeService.use('es');
  }
  onAddNewItem() {
    this.addNewItem.emit(null);
  }
  ngOnInit() {
    this.loadTruckData();
    this.loadDriverData();
    this.loadSiteData();

  }

  loadTruckData() {
    this.truckService.getAll().subscribe(
      data => {
        this.truck = data;
        this.trucks = this.truck.map(val => ({

          label: val.registrationNumber + "  || " + val.model,
          value: val
        }));


      }
    );


  }

  loadDriverData() {
    this.driverService.getAll().subscribe(
      data => {
        this.driver = data;
        this.drivers = this.driver.map(val => ({
          label: val.firstName + ' ' + val.lastName,
          value: val
        }));

      }
    );


  }

  loadSiteData() {
    this.siteService.getAll().subscribe(
      data => {
        this.site = data;
        this.sites = this.site.map(val => ({
          label: val.name + "  || " + val.longitude,
          value: val
        }));

      }
    );
  }

  save() {
    this.mission.drivers.forEach(element => {
      element.mission = null;
    });

    //  this.mission.drivers.forEach(element => {
    //     element.truck = this.truckmission;
    //  });
    this.site3.map(s => {
      this.mission.sites = [];
      this.mission.sites.push(this.site1, s, this.site2);

    });

    this.missionService
      .addMission(this.mission, this.mission.sites).subscribe(data => {
        this.mission = new Mission();
        this.modalRef.hide();


      },
        error => error);
  }

  updateMission() {
    this.mission.drivers.forEach(element => {
      element.mission = null;
    });
    this.mission.sites = [];
    this.mission.sites.push(this.site1);
    this.site3.map(s => {
      this.mission.sites.push(s);
    });
    this.mission.sites.push(this.site2);

    this.missionService.updateMission(this.mission)
      .subscribe(data => {
        this.mission = new Mission();
        this.modalRef.hide();
        this.gotoList();
      }, error => console.log(error));
  }
  show(mission: Mission) {

    this.mission = mission;
    this.drs = [];

    this.drivers.map(e => {
      this.mission.drivers.map(e1 => {
        if (e.value.id == e1.id) {
          this.drs.push(e);
        }
      })
    });
    this.trucks.map(e => {
      if (this.mission.drivers[0].truck.id == e.value.id) {
        this.truckmission = e.value.registrationNumber
      }
    });

    this.modalRef = this.modalService.show(this.template, this.config);

  }
  onSubmit() {
    this.submitted = true;
    this.updateMission();
  }
  addTagFn(addedName) {
    return { name: addedName, tag: true };
  }

  gotoList() {
    this.router.navigate(['app/missions/listMission']);
  }
}
