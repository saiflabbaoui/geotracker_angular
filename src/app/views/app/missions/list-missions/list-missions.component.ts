import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { ApiService } from 'src/app/data/api.service';
import { IProduct } from 'src/app/data/api.service';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AddMissionComponent } from '../add-mission/add-mission.component';
import { Mission } from 'src/app/models/mission';
import { MissionService } from 'src/app/services/mission.service';
import { AddSiteComponent } from '../../sites/add-site/add-site.component';
import { ColDef, GridApi, ColumnApi } from 'ag-grid-community';
import { UpdateMissionComponent } from '../update-mission/update-mission.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-list-missions',
  templateUrl: './list-missions.component.html'
})
export class ListMissionsComponent implements OnInit {

  displayMode = 'list';
  selectAllState = '';
  selected: Mission[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 5;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  missions: Observable<Mission[]>;
  mission: Mission[] = [];
  firstNameDriver
  public columnDefs: ColDef[];
  // gridApi and columnApi  
  private api: GridApi;
  private columnApi: ColumnApi;
  filterForm: FormGroup;
  nbrMissionEnCours: any;
  nbrMissioTermine: any;
  nbrMissionEnAttente: any;


  
  public rowData: any[];
  sideBar: any;

  isCollapsed = false;
  modalRef: BsModalRef;



  @Output() updateNewItem: EventEmitter<any> = new EventEmitter();

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddMissionComponent;
  @ViewChild('addNewSiteRef', { static: true }) addNewSiteRef: AddSiteComponent;
  @ViewChild('updateNewModalRef', { static: true }) updateNewModalRef: UpdateMissionComponent;

  constructor(private hotkeysService: HotkeysService, private missionService: MissionService, private modalService: BsModalService, private formBuilder: FormBuilder) {
    this.columnDefs = this.createColumnDefs();
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.mission];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
    this.sideBar = {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'columns',
          iconKey: 'columns',
          toolPanel: 'agColumnsToolPanel',
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filter',
          toolPanel: 'agFiltersToolPanel',
        }
      ],
      position: 'left',
      defaultToolPanel: 'filters'
    };
  }



  ngOnInit() {
    this.filterForm = this.formBuilder.group({
      enCours: [''],
      termine: [''],
      enAttente: ['']
    });

    this.missionService.getAll().subscribe(data => {
      this.mission = data;

    })

    this.missionService.nbrMissionsEnCours().subscribe(data=>{
      this.nbrMissionEnCours=data;
    });
    this.missionService.nbrMissionsTermine().subscribe(data=>{
      this.nbrMissioTermine=data;
    });
    this.missionService.nbrMissionsEnAttente().subscribe(data=>{
      this.nbrMissionEnAttente=data;
    });
    this.loadData(this.itemsPerPage, this.currentPage, this.search, this.orderBy);
    
  }

  onGridReady(params): void {
    this.api = params.api;
    this.columnApi = params.columnApi;
    this.api.sizeColumnsToFit();
  }
  private createColumnDefs() {
    return [{
      headerName: 'Start Date',
      field: 'startDate',
      filter: true,
      enableSorting: true,
      editable: true,
      sortable: true
    }, {
      headerName: 'End Date',
      field: 'endDate',
      filter: true,
      editable: true,
      sortable: true
    }, {
      headerName: 'Description',
      field: 'description',
      filter: true,
      sortable: true,
      editable: true,
      cellRenderer: '<a href="edit-user">{{email}}</a>'
    }, {
      headerName: 'Sites',
      field: 'sites',
      filter: true,
      editable: true,
      sortable: true
    }, {
      headerName: 'Drivers',
      field: 'drivers',
      filter: true,
      editable: true
    }]
  }

  loadData(pageSize: number = 5, currentPage: number = 1, search: string = '', orderBy: string = '') {
    this.itemsPerPage = pageSize;
    this.currentPage = currentPage;
    this.search = search;
    this.orderBy = orderBy;
    this.missionService.getAllMissions(pageSize, currentPage, search, orderBy).subscribe(
      data => {
        if (data != null) {
          this.isLoading = false;
          this.mission = data.data;
          this.firstNameDriver = this.mission.forEach(element => {
            element.drivers.forEach(element => {
              element.firstName
            });
          });
          this.totalItem = data.totalItem;
          this.totalPage = data.totalPage;
          this.setSelectAllState();
        } else {
          this.endOfTheList = true;
        }
      },
      error => {
        this.isLoading = false;
      }
    );

  }

  changeDisplayMode(mode) {
    this.displayMode = mode;
  }

  showAddNewModal() {
    this.addNewModalRef.show();
  }
  showAddNewSite() {
    this.addNewSiteRef.show();
  }

  isSelected(p: Mission) {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }
  onSelect(item: Mission) {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState() {
    if (this.selected.length === this.mission.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event) {
    if ($event.target.checked) {
      this.selected = [...this.mission];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  pageChanged(event: any): void {
    this.loadData(this.itemsPerPage, event.page, this.search, this.orderBy);
  }

  itemsPerPageChange(perPage: number) {
    this.loadData(perPage, 1, this.search, this.orderBy);
  }

  changeOrderBy(item: any) {
    this.loadData(this.itemsPerPage, 1, this.search, item.value);
  }

  searchKeyUp(event) {
    this.loadData(this.itemsPerPage, 1, event, this.orderBy);
  }

  onContextMenuClick(action: string, item: IProduct) {
    console.log('onContextMenuClick -> action :  ', action, ', item.title :', item.title);
  }
  updateMission(m: Mission) {
    this.updateNewModalRef.show(m);
  }

  onStateChange(m: Mission) {

    this.mission.push(m);

  }
 

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,{class: 'modal-lg'});
  }
  filterEtat(event) {
    this.missionService.getAll().subscribe(data => { this.mission = data });

    if (this.filterForm.controls['enCours'].value && this.filterForm.controls['terminé'].value) {
      this.missionService.getAll().subscribe(data => { this.mission = data.filter(x => x.status == "EnCours" || x.status == "Terminé") });
    } else if (this.filterForm.controls['enCours'].value && this.filterForm.controls['enAttente'].value) {
      this.missionService.getAll().subscribe(data => { this.mission = data.filter(x => x.status == "EnCours" || x.status == "EnAttente") });
    } else if (this.filterForm.controls['terminé'].value && this.filterForm.controls['enAttente'].value) {
      this.missionService.getAll().subscribe(data => { this.mission = data.filter(x => x.status == "Terminé" || x.status == "EnAttente") });
    } else {
      if (this.filterForm.controls['enCours'].value) {
        this.missionService.getAll().subscribe(data => { this.mission = data.filter(x => x.status == "EnCours") });
      } else if (this.filterForm.controls['terminé'].value) {
        this.missionService.getAll().subscribe(data => { this.mission = data.filter(x => x.status == "Terminé") });
      } else if (this.filterForm.controls['enAttente'].value) {
        this.missionService.getAll().subscribe(data => { this.mission = data.filter(x => x.status == "EnAttente") });
      }
    }

  
  }
}
