import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListMissionsComponent } from './list-missions/list-missions.component';
import { MissionsComponent } from './missions.component';


const routes: Routes = [ 
    {
        path: '', component: MissionsComponent,
        children: [
            { path: '', redirectTo: 'listMission', pathMatch: 'full' },
            { path: 'listMission', component: ListMissionsComponent },
          
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MissionsRoutingModule { }
