import { Component, TemplateRef, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { BsLocaleService, esLocale } from 'ngx-bootstrap';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { Observable } from 'rxjs';
import { TruckService } from 'src/app/services/truckService/truck.service';
import { Truck } from 'src/app/models/truck';
import { DriverService } from 'src/app/services/driver.service';
import { Driver } from 'src/app/models/driver';
import { MissionService } from 'src/app/services/mission.service';
import { Mission } from 'src/app/models/mission';
import { Site } from 'src/app/models/site';
import { SiteService } from 'src/app/services/site.service ';
import { AddSiteComponent } from '../../sites/add-site/add-site.component';
import { FormControl, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-mission',
  templateUrl: './add-mission.component.html',
  styles: []
})
export class AddMissionComponent implements OnInit {

  @Output() addNewItem: EventEmitter<any> = new EventEmitter();
  @Output() listMission = new EventEmitter<Mission>();

  submitted = false;
  docForm1: FormGroup;
  docForm2: FormGroup;
  minDate: Date;
  minDateStart: String;
  selectedStartDate: String;

  minDateEnd: String;
  changeDate: string;

  bsValue = new Date();
  missionStartDate: Date[];
  missionEndDate: Date[];

  maxDate = new Date();
  bsInlineValue = new Date();

  githubUsers$: Observable<any[]>;
  selectedUsers = [];

  currentPage = 1;
  itemsPerPage = 5;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  truck: Truck[] = [];
  truckmission: Truck;
  driver: Driver[] = [];
  site: Site[];
  mission: Mission = new Mission();
  site1: Site;
  site2: Site;
  site3: Site[];

  trucks = [];
  drivers = [];
  sites = [];


  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  constructor(private modalService: BsModalService, private truckService: TruckService,
    private driverService: DriverService, private siteService: SiteService, private missionService: MissionService, public datepipe: DatePipe) {
    this.changeDate = new Date().toISOString().slice(0, 16);

    const currentYear = new Date().getFullYear();
    this.minDate = new Date();


  }
  onAddNewItem() {
    this.addNewItem.emit(null);
  }
  onChangeDate($event) {
    this.selectedStartDate = $event;
    this.minDateEnd = this.datepipe.transform(this.selectedStartDate, 'yyyy-MM-ddTHH:MM');
  }
  ngOnInit() {
    let statDateTime = 'Jun 15, 2015, 21:43:11 UTC';
    let endDateTime = 'Jun 15, 2015, 21:43:11 UTC'; //You'll want to change this to UTC or it can mess up your date.
    this.docForm1 = new FormGroup({
      'startDateTime': new FormControl(new Date(statDateTime).toISOString().slice(0, -1)),
    });
    this.docForm2 = new FormGroup({
      'endDateTime': new FormControl(new Date(endDateTime).toISOString().slice(0, -1))
    });
    this.minDateStart = this.datepipe.transform(this.minDate, 'yyyy-MM-ddTHH:MM');
    this.minDateEnd = this.datepipe.transform(this.minDate, 'yyyy-MM-ddTHH:MM');


    this.loadTruckData();
    this.loadDriverData();
    this.loadSiteData();

  }

  loadTruckData() {
    this.truckService.getAll().subscribe(
      data => {
        this.truck = data;
        this.trucks = this.truck.filter(truck => truck.status === "Free").map(val => ({

          label: val.registrationNumber + "  || " + val.model,
          value: val
        }));


      }
    );


  }

  loadDriverData() {
    this.driverService.getAll().subscribe(
      data => {
        this.driver = data;
        this.drivers = this.driver.filter(driver => driver.truck == null).map(val => ({
          label: val.firstName + ' ' + val.lastName,
          value: val
        }));
      }
    );

  }

  loadSiteData() {
    this.siteService.getAll().subscribe(
      data => {
        this.site = data;
        this.sites = this.site.map(val => ({
          label: val.name + "  || " + val.longitude,
          value: val
        }));

      }
    );
  }

  save() {
    this.mission.drivers.forEach(element => {
      element.mission = null;
    });

    this.mission.drivers.forEach(element => {
      element.truck = this.truckmission;
    });
    this.mission.sites = [];
    this.mission.sites.push(this.site1);
    this.site3.map(s => {
      this.mission.sites.push(s);
    });
    this.mission.sites.push(this.site2);

    this.missionService
      .addMission(this.mission, this.mission.sites).subscribe(data => {
        this.mission = data;
        this.hide();


      },
        error => error);
  }
  show() {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
  hide() {
    if (this.mission.id != null) {
      this.listMission.emit(this.mission);
    }
    this.modalRef.hide();
  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }
  addTagFn(addedName) {
    return { name: addedName, tag: true };
  }
  onStateChange(s:Site) {
    this.sites.push(s);
  
  }
}
