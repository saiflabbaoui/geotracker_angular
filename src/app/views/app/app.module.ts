import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlankPageComponent } from './blank-page/blank-page.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { DriversComponent } from './drivers/drivers.component';
import { AgmCoreModule } from '@agm/core';
import { YaCoreModule, YamapngModule } from 'yamapng';
import { TranslateModule } from '@ngx-translate/core';
import { BootstrapModule } from 'src/app/components/bootstrap/bootstrap.module';
import { ComponentsStateButtonModule } from 'src/app/components/state-button/components.state-button.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { SortablejsModule } from 'ngx-sortablejs';
import { QuillModule } from 'ngx-quill';
import { UiModalsContainersModule } from 'src/app/containers/ui/modals/ui.modals.containers.module';
import { FormsModule } from '@angular/forms';
import { ComponentsCarouselModule } from 'src/app/components/carousel/components.carousel.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { UiCardsContainersModule } from 'src/app/containers/ui/cards/ui.cards.containers.module';
import { ComponentsRoutingModule } from './ui/components/components.routing';
import { MapsComponent } from './tracking/maps/maps.component';
import { AddSiteComponent } from './sites/add-site/add-site.component';
import { ListSitesComponent } from './sites/list-sites/list-sites.component';
import { ProfilComponent } from './profil/profil.component';
import { ArchwizardModule } from 'angular-archwizard';




@NgModule({
  declarations: [BlankPageComponent, AppComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    SharedModule,
    LayoutContainersModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDXp2UQHy2S-pyPqniPA--wSYM-MrLCdGg',
      libraries: ['places']

    }),
    FormsModule

  ]
})
export class AppModule { }

