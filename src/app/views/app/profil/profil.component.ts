import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Company } from 'src/app/models/company';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/userService/user.service';
import { AuthService } from 'src/app/shared/auth.service';
import { TokenStorageService } from 'src/app/shared/token-storage.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html'
})
export class ProfilComponent implements OnInit {
  currentUser: User ;
  form01: FormGroup;
  disableBtn=false;
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-center'
  };
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(
    private userService:UserService,
    private tokenStorage: TokenStorageService,
    private fb : FormBuilder, private modalService: BsModalService, public builder: FormBuilder) { }

  ngOnInit(){
//    this.currentUser.userName=sessionStorage.getItem("AuthUsername");
   // console.log(this.currentUser.userName);
   this.form01 = this.builder.group({
    registrationNumber: ["", Validators.required],
    address: ["", Validators.required],
    phoneNumber: ["", Validators.required],
    firstName: ["", Validators.required], 
    lastName: ["", Validators.required], 
    userName: ["", Validators.required], 
    email: ["", Validators.required], 

  });
  this. form01.valueChanges 
            .subscribe((changedObj: any) => {
                 this.disableBtn = this.form01.controls['firstName' || 'lastName' || 'userName'|| 'email'].touched;
            });
   this.getCurrentUser(this.tokenStorage.getUser());
 
   
  }

getCurrentUser(user:any){
  this.userService.getUserById(user['id']).subscribe(user=> {
    this.currentUser = user;
    console.log(this.currentUser);
  });
}

show() {

  this.modalRef = this.modalService.show(this.template, this.config);
}

submit() {
    
}
update(){
  this.userService.updateProfil(this.currentUser).subscribe(data=>{
  console.log(data);
  this.modalRef.hide();

  })
}

}
