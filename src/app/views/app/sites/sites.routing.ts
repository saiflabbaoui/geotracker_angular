import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListSitesComponent } from './list-sites/list-sites.component';
import { SitesComponent } from './sites.component';




const routes: Routes = [ 
    {
        path: '', component: SitesComponent,
        children: [
            { path: '', redirectTo: 'listSite', pathMatch: 'full' },
            { path: 'listSite', component: ListSitesComponent },
          
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SitesRoutingModule { }
