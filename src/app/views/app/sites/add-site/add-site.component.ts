import { google } from '@agm/core/services/google-maps-types';
import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Site } from 'src/app/models/site';
import { SiteService } from 'src/app/services/site.service ';

@Component({
  selector: 'app-add-site',
  templateUrl: './add-site.component.html'
})
export class AddSiteComponent implements OnInit {
  @Output() listSite = new EventEmitter<Site>();

  submitted = false;
  site: Site = new Site();
  addSiteFrom: FormGroup;

 

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-center',
    
  };



  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  map: any;
  mapClickListener: any;
  zone: any;
  constructor(private modalService: BsModalService, private siteService: SiteService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.addSiteFrom = this.formBuilder.group({
      latitude: ['', [Validators.required, Validators.pattern("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,15}")]],
      longitude: ['', [Validators.required, Validators.pattern("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,15}")]],
      name: ['', Validators.required]
      });
  }
  get f() { return this.addSiteFrom.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.addSiteFrom.valid){
    this.save();
    this.addSiteFrom.reset();
    
  }
}
  
  save() {
    
    this.siteService.addSite(this.site).subscribe(data => {
      this.site = data;

      this.hide();
    }, 
    error => error)

  }

  show() {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  placeMarker($event){
    console.log($event.coords.lat);
    this.site.latitude = $event.coords.lat;
    console.log($event.coords.lng);
    this.site.longitude = $event.coords.lng;

  }

  hide(){
    if(this.site.id!=null){
      this.listSite.emit(this.site);
    }

    this.modalRef.hide();
  }
}
