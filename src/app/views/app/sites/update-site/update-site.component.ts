import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Site } from 'src/app/models/site';
import { SiteService } from 'src/app/services/site.service ';

@Component({
  selector: 'app-update-site',
  templateUrl: './update-site.component.html'
})
export class UpdateSiteComponent implements OnInit {

  site: Site = new Site();
  submitted = false;

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-left'
  };
  


  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private siteService: SiteService, private router: Router) { }


  show(site: Site) {

    this.site=site;
     this.modalRef = this.modalService.show(this.template, this.config);
   }

  ngOnInit() {

  }

  updateSite() {
    this.siteService.updateSite(this.site)
      .subscribe(data => {
        console.log(data);
        this.site = new Site();
        this.modalRef.hide();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.updateSite();    
  }

  gotoList() {
    this.router.navigate(['app/sites/listSite']);
  }
  placeMarker($event){
    console.log($event.coords.lat);
    this.site.latitude = $event.coords.lat;
    console.log($event.coords.lng);
    this.site.longitude = $event.coords.lng;

  }

}
