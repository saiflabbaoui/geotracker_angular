import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  SitesRoutingModule } from './sites.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotkeyModule } from 'angular2-hotkeys';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { SortablejsModule } from 'ngx-sortablejs';
import { CollapseModule, TabsModule, ProgressbarModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { ContextMenuModule } from 'ngx-contextmenu';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SitesComponent } from './sites.component';
import { ListSitesComponent } from './list-sites/list-sites.component';
import { AddSiteComponent } from './add-site/add-site.component';
import { SiteHeaderComponent } from './site-header/site-header.component';
import { UpdateSiteComponent } from './update-site/update-site.component';
import { AgmCoreModule } from '@agm/core';
import { ArchwizardModule } from 'angular-archwizard';


@NgModule({
  declarations: [SitesComponent, ListSitesComponent, SiteHeaderComponent, UpdateSiteComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    SitesRoutingModule,
    PagesContainersModule,
    LayoutContainersModule,
    ApplicationsContainersModule,
    ComponentsChartModule,
    SortablejsModule,
    NgSelectModule,
    PaginationModule.forRoot(),
    HotkeyModule.forRoot(),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true,
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDXp2UQHy2S-pyPqniPA--wSYM-MrLCdGg'
    }),
    ArchwizardModule


  ]
})
export class SitesModule { }
