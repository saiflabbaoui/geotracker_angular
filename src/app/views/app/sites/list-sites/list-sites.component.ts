import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { ApiService } from 'src/app/data/api.service';
import { IProduct } from 'src/app/data/api.service';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { Observable } from 'rxjs';
import { Driver } from 'src/app/models/driver';
import { AddSiteComponent } from '../add-site/add-site.component';
import { UpdateSiteComponent } from '../update-site/update-site.component';
import { SiteService } from 'src/app/services/site.service ';
import { Site } from 'src/app/models/site';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-list-sites',
  templateUrl: './list-sites.component.html'
})
export class ListSitesComponent implements OnInit {

  displayMode = 'list';
  selectAllState = '';
  selected: Site[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 5;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  sites: Observable<Site[]>;
  site: Site[] = [];
  modalRef: BsModalRef;

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddSiteComponent;
  @ViewChild('updateNewModalRef', { static: true }) updateNewModalRef: UpdateSiteComponent;

  constructor(private hotkeysService: HotkeysService, private siteService: SiteService, private modalService: BsModalService) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.site];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }


  ngOnInit() {
    this.loadData(this.itemsPerPage, this.currentPage, this.search, this.orderBy);
  }

  loadData(pageSize: number = 5, currentPage: number = 1, search: string = '', orderBy: string = '') {

    this.itemsPerPage = pageSize;
    this.currentPage = currentPage;
    this.search = search;
    this.orderBy = orderBy;
    this.siteService.getAllSitesPagean(pageSize, currentPage, search, orderBy).subscribe(
      data => {
        if (data!=null) {
        this.isLoading = false;
        this.site = data.data;
        this.totalItem = data.totalItem;
        this.totalPage = data.totalPage;
        this.setSelectAllState();
      }else {
        this.endOfTheList = true;
      }
      },
      error => {
        this.isLoading = false;
      }
    );

  
  }

  changeDisplayMode(mode) {
    this.displayMode = mode;
  }

  showAddNewModal() {
    this.addNewModalRef.show();
  }

  isSelected(p: Site) {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }
  onSelect(item: Site) {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState() {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event) {
    if ($event.target.checked) {
      this.selected = [...this.site];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  pageChanged(event: any): void {
    this.loadData(this.itemsPerPage, event.page, this.search, this.orderBy);
  }

  itemsPerPageChange(perPage: number) {
    this.loadData(perPage, 1, this.search, this.orderBy);
  }

  changeOrderBy(item: any) {
    this.loadData(this.itemsPerPage, 1, this.search, item.value);
  }

  searchKeyUp(event) {
    const val = event.target.value.toLowerCase().trim();
    this.loadData(this.itemsPerPage, 1, val, this.orderBy);
  }

  onContextMenuClick(action: string, item: IProduct) {
    console.log('onContextMenuClick -> action :  ', action, ', item.title :', item.title);
  }
  updateSite(p:Site){
    this.updateNewModalRef.show(p);
  }

  deleteSite( template: TemplateRef<any>    ){
   
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
   
  }
  confirm(p: Driver): void {
    this.siteService.deleteSite(p.id)
    .subscribe(data => {
      console.log("aaaa",data);
this.loadData();     
    }, error => console.log(error));
        this.modalRef.hide();
  }

  decline(): void {
    this.modalRef.hide();
  }

  onStateChange(s:Site) {
    this.site.push(s);
  
  }

}
