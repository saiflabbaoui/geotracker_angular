import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Driver } from 'src/app/models/driver';
import { DriverService } from 'src/app/services/driver.service';

@Component({
  selector: 'app-update-driver',
  templateUrl: './update-driver.component.html'
})
export class UpdateDriverComponent implements OnInit {

  driver: Driver = new Driver();
  submitted = false;

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-left'
  };
  


  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private driverService: DriverService, private router: Router) { }


  show(driver: Driver) {

    this.driver=driver;
     this.modalRef = this.modalService.show(this.template, this.config);
   }

  ngOnInit() {

  }

  updateDriver() {
    this.driverService.updateDriver(this.driver)
      .subscribe(data => {
        this.driver = new Driver();
        this.modalRef.hide();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.updateDriver();    
  }

  gotoList() {
    this.router.navigate(['app/drivers/listDriver']);
  }
 
}
