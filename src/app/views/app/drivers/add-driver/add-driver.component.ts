import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Driver } from 'src/app/models/driver';
import { User } from 'src/app/models/user';
import { DriverService } from 'src/app/services/driver.service';
import { UserService } from 'src/app/services/userService/user.service';
import { TokenStorageService } from 'src/app/shared/token-storage.service';

@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html'
})
export class AddDriverComponent implements OnInit {
  @Output() listDriver = new EventEmitter<Driver>();

  driver: Driver = new Driver();
  submitted = false;
  addDriverFrom: FormGroup;
  error;

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };



  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  currentUser: User ;

  constructor(private modalService: BsModalService, private driverService: DriverService, private formBuilder: FormBuilder,private userService:UserService,
    private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    this.addDriverFrom = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{8}$")]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required]

    });
    this.getCurrentUser(this.tokenStorage.getUser());

  }

  get f() { return this.addDriverFrom.controls; }

  getCurrentUser(user:any){
    this.userService.getUserById(user['id']).subscribe(user=> {
      this.currentUser = user;
    });
  }
  save() {
    this.error = null
    this.driver.company=this.currentUser.company;
    this.driverService.addDriver(this.driver).subscribe(data => {
      this.driver = data;
      this.hide();

    }, (error) => {
      this.error = error});

  }
  onSubmit() {
    this.submitted = true;
    if (this.addDriverFrom.valid) {
      this.save();

    }
    this.addDriverFrom.reset();

  }
  show() {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
  hide() {
    if (this.driver.id != null) {
      this.listDriver.emit(this.driver);
    }
    this.modalRef.hide();
  }
}
