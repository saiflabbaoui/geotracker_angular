import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriversRoutingModule } from './drivers.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotkeyModule } from 'angular2-hotkeys';
import { ApplicationsContainersModule } from 'src/app/containers/applications/applications.containers.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { SortablejsModule } from 'ngx-sortablejs';
import { CollapseModule, TabsModule, ProgressbarModule, ModalModule, PaginationModule } from 'ngx-bootstrap';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { DriversComponent } from './drivers.component';
import { ListDriversComponent } from './list-drivers/list-drivers.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DriverHeaderComponent } from './driver-header/driver-header.component';
import { UpdateDriverComponent } from './update-driver/update-driver.component';

@NgModule({
  declarations: [DriversComponent, ListDriversComponent, AddDriverComponent, DriverHeaderComponent, UpdateDriverComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    DriversRoutingModule,
    PagesContainersModule,
    LayoutContainersModule,
    ApplicationsContainersModule,
    ComponentsChartModule,
    SortablejsModule,
    NgSelectModule,
    PaginationModule.forRoot(),
    HotkeyModule.forRoot(),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    ProgressbarModule.forRoot(),
    ModalModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true,
    }),
    ReactiveFormsModule


  ]
})
export class DriversModule { }
