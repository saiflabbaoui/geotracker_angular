import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { ApiService } from 'src/app/data/api.service';
import { IProduct } from 'src/app/data/api.service';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { Observable } from 'rxjs';
import { AddDriverComponent } from '../add-driver/add-driver.component';
import { Driver } from 'src/app/models/driver';
import { DriverService } from 'src/app/services/driver.service';
import { UpdateDriverComponent } from '../update-driver/update-driver.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
@Component({
  selector: 'app-list-drivers',
  templateUrl: './list-drivers.component.html'
})
export class ListDriversComponent implements OnInit {

  displayMode = 'list';
  selectAllState = '';
  selected: Driver[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 5;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  drivers: Observable<Driver[]>;
  driver: Driver[] = [];

  modalRef: BsModalRef;

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddDriverComponent;
  @ViewChild('updateNewModalRef', { static: true }) updateNewModalRef: UpdateDriverComponent;

  constructor(private hotkeysService: HotkeysService, private apiService: ApiService, private driverService: DriverService, private modalService: BsModalService) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.driver];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }


  ngOnInit() {
    this.loadData(this.itemsPerPage, this.currentPage, this.search, this.orderBy);
  }

  loadData(pageSize: number = 5, currentPage: number = 1, search: string = '', orderBy: string = '') {

    this.itemsPerPage = pageSize;
    this.currentPage = currentPage;
    this.search = search;
    this.orderBy = orderBy;
    this.driverService.getAlldrivers(pageSize, currentPage, search, orderBy).subscribe(
      data => {
        if (data!=null) {
        this.isLoading = false;
        this.driver = data.data;
        this.totalItem = data.totalItem;
        this.totalPage = data.totalPage;
        this.setSelectAllState();
      }else {
        this.endOfTheList = true;
      }
      },
      error => {
        this.isLoading = false;
      }
    );

  
  }

  changeDisplayMode(mode) {
    this.displayMode = mode;
  }

  showAddNewModal() {
    this.addNewModalRef.show();
  }

  isSelected(p: Driver) {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }
  onSelect(item: Driver) {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState() {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event) {
    if ($event.target.checked) {
      this.selected = [...this.driver];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  pageChanged(event: any): void {
    this.loadData(this.itemsPerPage, event.page, this.search, this.orderBy);
  }

  itemsPerPageChange(perPage: number) {
    this.loadData(perPage, 1, this.search, this.orderBy);
  }

  changeOrderBy(item: any) {
    this.loadData(this.itemsPerPage, 1, this.search, item.value);
  }

  searchKeyUp(event) {
    this.loadData(this.itemsPerPage, 1, event, this.orderBy);
  }

  onContextMenuClick(action: string, item: IProduct) {
    console.log('onContextMenuClick -> action :  ', action, ', item.title :', item.title);
  }
  updateDriver(p:Driver){
    this.updateNewModalRef.show(p);
  }


  deleteDriver( template: TemplateRef<any>    ){
   
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
   
  }
  confirm(p: Driver): void {
    this.driverService.deleteDriver(p.id)
    .subscribe(data => {
      console.log("aaaa",data);
this.loadData();     
    }, error => console.log(error));    this.modalRef.hide();
  }

  decline(): void {
    this.modalRef.hide();
  }
  
  onStateChange(d:Driver) {
    this.driver.push(d);
  
  }
}
