import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriversComponent } from './drivers.component';
import { ListDriversComponent } from './list-drivers/list-drivers.component';



const routes: Routes = [ 
    {
        path: '', component: DriversComponent,
        children: [
            { path: '', redirectTo: 'listDriver', pathMatch: 'full' },
            { path: 'listDriver', component: ListDriversComponent },
          
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DriversRoutingModule { }
