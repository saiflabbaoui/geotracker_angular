import { NgModule } from '@angular/core';

import { TrucksComponent } from './trucks.component';
import { TrucksRoutingModule } from './trucks.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardsContainersModule } from 'src/app/containers/dashboards/dashboards.containers.module';
import { ComponentsCardsModule } from 'src/app/components/cards/components.cards.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { AddTruckComponent } from './add-truck/add-truck.component';
import { FormsModule, FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { ListTrucksComponent } from './list-trucks/list-trucks.component';
import { CollapseModule, AccordionModule, BsDropdownModule, ModalModule, PaginationModule, RatingModule, TabsModule } from 'ngx-bootstrap';
import { ProductComponent } from '../pages/product/product.component';
import { ProductRoutingModule } from '../pages/product/product.routing';
import { ComponentsCarouselModule } from 'src/app/components/carousel/components.carousel.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { HotkeyModule } from 'angular2-hotkeys';
import { ContextMenuModule } from 'ngx-contextmenu';
import { NgSelectModule } from '@ng-select/ng-select';
import { TruckHeaderComponent } from './truck-header/truck-header.component';
import { UpdateTruckComponent } from './update-truck/update-truck.component';

@NgModule({
  declarations: [AddTruckComponent, TrucksComponent, ListTrucksComponent, TruckHeaderComponent, UpdateTruckComponent],
  imports: [
    SharedModule,
    LayoutContainersModule,
    DashboardsContainersModule,
    TrucksRoutingModule,
    ComponentsCardsModule,
    FormsModule,
    ProductRoutingModule,
    ComponentsCarouselModule,
    PagesContainersModule,
    ComponentsCardsModule,
    ComponentsChartModule,
    RatingModule,
    CollapseModule,
    NgSelectModule,
    FormsModuleAngular,
    ReactiveFormsModule,
    HotkeyModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true,
    })

    
  ]
})
export class TrucksModule { }
