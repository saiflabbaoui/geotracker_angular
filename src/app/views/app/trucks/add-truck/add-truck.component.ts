import { Component, TemplateRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Truck } from 'src/app/models/truck';
import { TruckService } from 'src/app/services/truckService/truck.service';
import { ListTrucksComponent } from '../list-trucks/list-trucks.component';
import { Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-truck',
  templateUrl: './add-truck.component.html',
})
export class AddTruckComponent implements OnInit {
  @Output() listtruck = new EventEmitter<Truck>();
  

  truck: Truck = new Truck();
  submitted = false;
  addTruckFrom: FormGroup;
  error;

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  categories = [
    { label: 'Renault', value: 'Renault' },
    { label: 'Mercedes-Benz', value: 'Mercedes-Benz' },
    { label: 'Caterpillar', value: 'Caterpillar' },
    { label: 'Volvo', value: 'Volvo' },
    { label: 'MAN', value: 'MAN' },
    { label: 'Scania', value: 'Scania' },
    { label: 'Iveco', value: 'Iveco' },
    { label: 'DAF', value: 'DAF' },
    { label: 'International', value: 'International' },
    { label: 'Peterbilt', value: 'Peterbilt' },
    { label: 'United', value: 'United' },
    { label: 'Ford', value: 'Ford' },
    { label: 'Freightliner', value: 'Freightliner' },
    { label: 'GMC', value: 'GMC' },
    { label: 'Kenworth', value: 'Kenworth' },
    { label: 'Mack', value: 'Mack' },
    { label: 'Isuzu', value: 'Isuzu' },
    { label: 'Chevrolet', value: 'Chevrolet' },
    { label: 'Ledwell', value: 'Ledwell' },
    { label: 'Bell', value: 'Bell' },
    { label: 'Sterling', value: 'Sterling' },
    { label: 'Astra', value: 'Astra' },
    { label: 'DFAC', value: 'DFAC' },
    { label: 'Oshkosh', value: 'Oshkosh' },
    { label: 'AM General', value: 'AM General' },
    { label: 'Feldbinder', value: 'Feldbinder' },
    { label: 'Nfp-Eurotrailer', value: 'Nfp-Eurotrailer' },
    { label: 'Shacman', value: 'Shacman' },
    { label: 'Sinotruk', value: 'Sinotruk' },
    { label: 'Tatra', value: 'Tatra' },
    { label: 'Volvo BM', value: 'Volvo BM' },
    { label: 'Agria', value: 'Agria' },
    { label: 'Deutz', value: 'Deutz' },
    { label: 'ERF', value: 'ERF' },
    { label: 'Ginaf', value: 'Ginaf' },
    { label: 'Hanomag', value: 'Hanomag' },
    { label: 'Hino', value: 'Hino' },
    { label: 'Howo', value: 'Howo' },
    { label: 'John Deere', value: 'John Deere' },
    { label: 'Linde', value: 'Linde' },
    { label: 'Mega', value: 'Mega' },
    { label: 'Nissan', value: 'Nissan' },
    { label: 'Pegaso', value: 'Pegaso' },
    { label: 'Rohr', value: 'Rohr' },
    { label: 'Skoda', value: 'Skoda' },
    { label: 'Spitzer', value: 'Spitzer' },
    { label: 'THOMPSON', value: 'THOMPSON' },
    { label: 'Titan', value: 'Titan' },
    { label: 'Welgro', value: 'Welgro' },
    { label: 'Western Star', value: 'Western Star' },



  ];


  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private truckService: TruckService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.addTruckFrom = this.formBuilder.group({
      registrationNumber: ['', Validators.required],
      model: ['', Validators.required],
      power: ['', Validators.required]
      });
  }
  get f() { return this.addTruckFrom.controls; }

   save() {
    this.error = null
    this.truckService
    .addTruck(this.truck).subscribe(data => {
      this.truck =data;
      
      this.hide();
      this.addTruckFrom.reset();

      
    }, (error) => {
      this.error = error});
  }
  onSubmit() {
    this.submitted = true;
    if (this.addTruckFrom.valid){
    this.save();    
  }
}
  show() {
    this.modalRef = this.modalService.show(this.template, this.config);
  }
 hide(){
   
   if(this.truck.id!=null){
     this.listtruck.emit(this.truck);
   }
   this.modalRef.hide();
 }
}
