import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { ApiService } from 'src/app/data/api.service';
import { IProduct } from 'src/app/data/api.service';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { AddTruckComponent } from '../add-truck/add-truck.component';
import { TruckService } from 'src/app/services/truckService/truck.service';
import { Observable } from 'rxjs';
import { Truck } from 'src/app/models/truck';
import { Router } from '@angular/router';
import { UpdateTruckComponent } from '../update-truck/update-truck.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
@Component({
  selector: 'app-list-trucks',
  templateUrl: './list-trucks.component.html',
})
export class ListTrucksComponent implements OnInit {
  displayMode = 'list';
  selectAllState = '';
  selected: Truck[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 5;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  trucks: Observable<Truck[]>;
  truck: Truck[] = [];
  modalRef: BsModalRef;

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddTruckComponent;
  @ViewChild('updateNewModalRef', { static: true }) updateNewModalRef: UpdateTruckComponent;

  constructor(private hotkeysService: HotkeysService, private apiService: ApiService, private truckService: TruckService, private router: Router, private modalService: BsModalService) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.truck];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }


   ngOnInit() {
     this.loadData(this.itemsPerPage, this.currentPage, this.search, this.orderBy);
      console.log(this.truck);

  }

   loadData(pageSize: number = 5, currentPage: number = 1, search: string = '', orderBy: string = '') {
    this.itemsPerPage = pageSize;
    this.currentPage = currentPage;
    this.search = search;
    this.orderBy = orderBy;
    this.truckService.getAllTrucks(pageSize, currentPage, search, orderBy).subscribe(
      data => {
        if (data!=null) {
        this.isLoading = false;
        this.truck = data.data;
        this.totalItem = data.totalItem;
        this.totalPage = data.totalPage;
        this.setSelectAllState();

      } else {
        this.endOfTheList = true;
      }
      },
      error => {
        this.isLoading = false;
      }
    );

    
  }

  changeDisplayMode(mode) {
    this.displayMode = mode;
  }

  showAddNewModal() {
    this.addNewModalRef.show();
  }

  isSelected(p: Truck) {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }
  onSelect(item: Truck) {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState() {
    if (this.selected.length === this.truck.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event) {
    if ($event.target.checked) {
      this.selected = [...this.truck];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  pageChanged(event: any): void {
    this.loadData(this.itemsPerPage, event.page, this.search, this.orderBy);
  }

  itemsPerPageChange(perPage: number) {
    this.loadData(perPage, 1, this.search, this.orderBy);
  }

  changeOrderBy(item: any) {
    this.loadData(this.itemsPerPage, 1, this.search, item.value);
  }

  searchKeyUp(event) {
    console.log(event);

    this.loadData(this.itemsPerPage, 1, event, this.orderBy);
  }

  onContextMenuClick(action: string, item: IProduct) {
    console.log('onContextMenuClick -> action :  ', action, ', item.title :', item.title);
  }
  updateTruck(p:Truck){
    this.updateNewModalRef.show(p);
  }

  deleteTruck( template: TemplateRef<any>    ){
   
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
   
  }
  confirm(p: Truck): void {
    this.truckService.deleteTruck(p.id)
    .subscribe(data => {
this.loadData();     
    }, error => console.log(error)); 
     this.modalRef.hide();
  }

  decline(): void {
    this.modalRef.hide();
  }

 onStateChange(t:Truck) {
  this.truck.push(t);
  

}
}
