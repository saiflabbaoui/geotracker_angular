import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { id } from 'date-fns/locale';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Truck } from 'src/app/models/truck';
import { TruckService } from 'src/app/services/truckService/truck.service';

@Component({
  selector: 'app-update-truck',
  templateUrl: './update-truck.component.html'
})
export class UpdateTruckComponent implements OnInit {

  id: number;
  truck: Truck = new Truck();
  submitted = false;

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-left'
  };
  categories = [
    { label: 'Renault', value: 'Renault' },
    { label: 'Mercedes-Benz', value: 'Mercedes-Benz' },
    { label: 'Caterpillar', value: 'Caterpillar' },
    { label: 'Volvo', value: 'Volvo' },
    { label: 'MAN', value: 'MAN' },
    { label: 'Scania', value: 'Scania' },
    { label: 'Iveco', value: 'Iveco' },
    { label: 'DAF', value: 'DAF' },
    { label: 'International', value: 'International' },
    { label: 'Peterbilt', value: 'Peterbilt' },
    { label: 'United', value: 'United' },
    { label: 'Ford', value: 'Ford' },
    { label: 'Freightliner', value: 'Freightliner' },
    { label: 'GMC', value: 'GMC' },
    { label: 'Kenworth', value: 'Kenworth' },
    { label: 'Mack', value: 'Mack' },
    { label: 'Isuzu', value: 'Isuzu' },
    { label: 'Chevrolet', value: 'Chevrolet' },
    { label: 'Ledwell', value: 'Ledwell' },
    { label: 'Bell', value: 'Bell' },
    { label: 'Sterling', value: 'Sterling' },
    { label: 'Astra', value: 'Astra' },
    { label: 'DFAC', value: 'DFAC' },
    { label: 'Oshkosh', value: 'Oshkosh' },
    { label: 'AM General', value: 'AM General' },
    { label: 'Feldbinder', value: 'Feldbinder' },
    { label: 'Nfp-Eurotrailer', value: 'Nfp-Eurotrailer' },
    { label: 'Shacman', value: 'Shacman' },
    { label: 'Sinotruk', value: 'Sinotruk' },
    { label: 'Tatra', value: 'Tatra' },
    { label: 'Volvo BM', value: 'Volvo BM' },
    { label: 'Agria', value: 'Agria' },
    { label: 'Deutz', value: 'Deutz' },
    { label: 'ERF', value: 'ERF' },
    { label: 'Ginaf', value: 'Ginaf' },
    { label: 'Hanomag', value: 'Hanomag' },
    { label: 'Hino', value: 'Hino' },
    { label: 'Howo', value: 'Howo' },
    { label: 'John Deere', value: 'John Deere' },
    { label: 'Linde', value: 'Linde' },
    { label: 'Mega', value: 'Mega' },
    { label: 'Nissan', value: 'Nissan' },
    { label: 'Pegaso', value: 'Pegaso' },
    { label: 'Rohr', value: 'Rohr' },
    { label: 'Skoda', value: 'Skoda' },
    { label: 'Spitzer', value: 'Spitzer' },
    { label: 'THOMPSON', value: 'THOMPSON' },
    { label: 'Titan', value: 'Titan' },
    { label: 'Welgro', value: 'Welgro' },
    { label: 'Western Star', value: 'Western Star' },



  ];


  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private route: ActivatedRoute, private truckService: TruckService, private router: Router) { }




  
  show(truck: Truck) {

   this.truck=truck;
    this.modalRef = this.modalService.show(this.template, this.config);
  }
  

ngOnInit() {
    
   // this.id = this.route.snapshot.params['id'];
  }

  updateTruck() {
    this.truckService.updateTruck(this.truck)
      .subscribe(data => {
        console.log(data);
        this.truck = new Truck();
        this.modalRef.hide();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.submitted = true;
    this.updateTruck();    
  }

  gotoList() {
    this.router.navigate(['app/trucks/listTruck']);
  }

}
