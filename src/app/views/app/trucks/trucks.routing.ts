import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTruckComponent } from './add-truck/add-truck.component';
import { ListTrucksComponent } from './list-trucks/list-trucks.component';
import { TrucksComponent } from './trucks.component';
import { UpdateTruckComponent } from './update-truck/update-truck.component';


const routes: Routes = [
    {
        path: '', component: TrucksComponent,
        children: [
            { path: '', redirectTo: 'listTruck', pathMatch: 'full' },
            { path: 'listTruck', component: ListTrucksComponent },


           
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TrucksRoutingModule { }