import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../models/role.user';
import { User } from '../models/user';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-views',
  templateUrl: './views.component.html'
})
export class ViewsComponent implements OnInit {
  currentUser: User;

  constructor(private router: Router, private authenticationService: AuthService) {
    // If you have landing page, remove below line and implement it here.
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.router.navigateByUrl('/user');
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role[''] === Role.Admin;
}

logout() {
    this.authenticationService.logOut();
    this.router.navigate(['/user']);
}
  ngOnInit() {

  }

}
