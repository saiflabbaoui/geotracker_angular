import { style } from "@angular/animations";
import { Color } from "ag-grid-community";

export interface IMenuItem {
  id?: string;
  icon?: string;
  label: string;
  to: string;
  newWindow?: boolean;
  subs?: IMenuItem[];
}

const data: IMenuItem[] = [
  {
    id: 'Settings',
    icon: 'iconsminds-security-settings',
    label: 'menu.settings',
    to: '/app/trucks',
    subs: [{
      icon: 'iconsminds-jeep',
      label: 'menu.trucks',
      to: '/app/trucks/listTruck'
    },{
      icon: 'iconsminds-mens',
      label: 'menu.drivers',
      to: '/app/drivers/listDriver'
    },{
      icon: 'iconsminds-location-2',
      label: 'menu.sites',
      to: '/app/sites/listSite'
    }
    ]
  }, {
    id: 'Missions',
    icon: 'iconsminds-edit-map',
    label: 'menu.missions',
    to: '/app/missions',
    subs: [{
      icon: 'iconsminds-edit-map',
      label: 'menu.missions',
      to: '/app/missions/listMission'
    }
    ]
  }, {
    id: 'Map',
    icon: 'iconsminds-map2',
    label: 'menu.tracker',
    to: '/app/maps',
    subs: [{
      icon: 'iconsminds-map2',
      label: 'menu.tracker',
      to: '/app/tracking/map'
    },
    {
      icon: 'iconsminds-map2',
      label: 'menu.historic',
      to: '/app/tracking/historic'
    }
    ]
  }];
export default data;
